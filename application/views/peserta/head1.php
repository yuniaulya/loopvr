
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Login Peserta</title>
<link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo-1.png'?>">
<!-- Bootstrap CSS -->
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- <link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.min.css'?>"> -->
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url().'theme/css/font-awesome.min.css'?>">
<!-- Simple Line Font -->
<link rel="stylesheet" href="<?php echo base_url().'theme/css/simple-line-icons.css'?>">
<!-- Owl Carousel -->
<link rel="stylesheet" href="<?php echo base_url().'theme/css/slick.css'?>">
<link rel="stylesheet" href="<?php echo base_url().'theme/css/slick-theme.css'?>">
<link rel="stylesheet" href="<?php echo base_url().'theme/css/owl.carousel.min.css'?>">
<link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<!-- Main CSS -->
<link href="<?php echo base_url().'theme/css/style.css'?>" rel="stylesheet">
<link rel="icon" type="image/png" href="<?php echo base_url().'images/icons/favicon.ico'?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fonts/iconic/css/material-design-iconic-font.min.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/animate/animate.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/css-hamburgers/hamburgers.min.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/animsition/css/animsition.min.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/select2/select2.min.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/daterangepicker/daterangepicker.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/noui/nouislider.min.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/util.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/main.css'?>">
<style type="text/css">
   .left    { text-align: left;}
   .right   { text-align: right;}
   .center  { text-align: center;}
   .justify { text-align: justify;}
</style>
