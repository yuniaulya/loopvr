<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}
	function index(){
			$this->load->view('depan/login_view');
		}

	function auth(){
$username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
$password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
$cek_user=$this->Login_model->auth_user($username,$password);
       if($cek_user->num_rows() > 0){ //jika login sebagai peserta
           $data=$cek_user->row_array();
           if($data['status']=='1'){ //Akses status
             $this->session->set_userdata('masuk',TRUE);
               $this->session->set_userdata('akses','3');
							 $this->session->set_userdata('emp_id',$data['emp_id']);
							 $this->session->set_userdata('ses_id',$data['email']);
							 $this->session->set_userdata('ses_nama',$data['emp_name']);
               redirect('peserta/person');
						 }
     }else
		 {  // jika username dan password tidak ditemukan atau salah
		$url=base_url();
		echo $this->session->set_flashdata('msg','Username Atau Password Salah !!');
		redirect('Login');
}
}


function logout(){
$this->session->sess_destroy();
$url=base_url('');
redirect($url);
}
	}
