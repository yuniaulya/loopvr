<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
<?php $this->load->view('depan/head') ?>
    <?php
        function limit_words($string, $word_limit){
            $words = explode(" ",$string);
            return implode(" ",array_splice($words,0,$word_limit));
        }
    ?>
</head>
<body>
<?php $this->load->view('depan/header') ?>
<section>
    <div class="slider_img layout_two">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <!-- <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol> -->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="d-block" src="<?php echo base_url().'theme/images/pic1.png'?>" alt="First slide">
                    <div class="carousel-caption d-md-block">
                        <div class="slider_title">
                        <br/> <br/>  <br/> <br/>  <br/> <br/>  <br/> <br/> <br/>
                           <!--  <h1>Bepikir Kreaftif &amp; Inovatif</h1>
                            <h4>Bagi kami kreativitas merupakan gerbang masa depan.<br> kreativitas akan mendorong inovasi. <br> Itulah yang kami lakukan.</h4> -->
                            <div class="slider-btn">
                                <a href="<?php echo site_url('login');?>" class="btn btn-primary">DAFTAR SEKARANG</a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
<!--//END HEADER -->
<!--============================= ABOUT =============================-->
<section class="clearfix about about-style2">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
               <h2>iLOOP Virtual Run</h2>
               <p>iLoop Virtual Run adalah event lari yang bisa dilakukan dimanapun dan kapan pun.
Lakukan pendaftaran secara online, pilih event, dan mulailah berlari, jogging atau berjalan.
Upload hasil dan beberapa hari kemudian finisher medal akan dikirim langsung ke rumahmu..</p>

            </div>
            <div class="col-md-4">
                <img src="<?php echo base_url().'theme/images/piala.jpg'?>" class="img-fluid about-img" alt="#">
            </div>
        </div>
    </div>
</section>
<!--//END ABOUT -->
<!--============================= OUR COURSES =============================-->
<section class="our_courses">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Race Event</h2>
            </div>
        </div>
        <div class="row">
          <?php foreach ($berita->result() as $row) :?>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" class="img-fluid" alt="courses-img">
                    </div>
                    <!-- // end .course-img-wrap -->
                    <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>" class="course-box-content">
                        <h3 style="text-align:center;"><?php echo $row->tulisan_judul;?></h3>
                    </a>
                </div>
            </div>
          <?php endforeach;?>
        </div> <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="<?php echo site_url('artikel');?>" class="btn btn-default btn-courses">View More</a>
            </div>
        </div>
    </div>
</section>
<!--//END OUR COURSES -->
<!--============================= EVENTS =============================-->

<!--//END EVENTS -->
<!--============================= DETAILED CHART =============================-->

<!--//END DETAILED CHART -->
<!--============================= FOOTER =============================-->
<?php $this->load->view('depan/footer') ?>
    </body>

    </html>
