<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <?php $this->load->view('peserta/head1') ?>
    <script>

    function startCalc(){
    interval = setInterval("calc()",1);}
    function calc(){
    km = document.autoSumForm.km.value;
    jam = document.autoSumForm.jam.value;
    menit = document.autoSumForm.menit.value;
    document.autoSumForm.point.value = (km * 100) - ((jam * 10) + (menit * 1));}
    function stopCalc(){
    clearInterval(interval);}
    </script>
</head>
<body>
  <?php $this->load->view('peserta/header1') ?>
  <?php
    $tgl_input = date("Y-m-d");
  ?>
<div>
  <div>
    <form  name='autoSumForm' class="contact100-form validate-form" <?php echo form_open_multipart('peserta/Dashboard/aksi_upload');?>
      <span class="col-md-12">
        <center><h2>Upload Your Result</h2></center>
        <center><h6>Upload hasil larimu, segera setelah berlari, akumulasi pada event yang di ikuti pada waktu yang ditentukan</h6></center>
        <br>
      </span>

      <input type="hidden" name="akun" value="<?php echo $this->session->userdata('ses_id');?>">
        <input type="hidden" name="tgl_input"  value="<?= $tgl_input; ?>" required="" readonly="">

      <div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
        <span class="label-input10">DISTANCE IN KM *</span>
        <input class="input100" type="number" name="km" placeholder="ex : 15,6"  onFocus="startCalc();" onBlur="stopCalc();" required>
      </div>

      <div class="wrap-input100 validate-input bg1 rs1-wrap-input100">
        <span class="label-input10">JAM (HH) *</span>
        <input class="input100" type="number" name="jam" placeholder="ex : 3 = 3 JAM"  onFocus="startCalc();" onBlur="stopCalc();" required>

        <span class="label-input10">MENIT (MM) *</span>
        <input class="input100" type="number" name="menit" placeholder="ex : 15 = 15 MENIT"  onFocus="startCalc();" onBlur="stopCalc();" required>
      </div>

      <div class="wrap-input100 bg1 rs1-wrap-input100">
     <span class="label-input10">POINT RUN</span>
     <input class="input100" type="text" name="point" onchange='tryNumberFormat(this.form.thirdBox);'  placeholder=" 0 " readonly required>
    </div>

      <div class="wrap-input100 input100-select bg1">
        <span class="label-input100">EVENT *</span>
        <div>
          <?php
          echo "<select name='event' class='js-select2' required> <option value='' disabled selected>Pilih Event</option>";
                       foreach ($event->result() as $row) {
          echo "<option value='".$row->kategori_nama."'>".$row->kategori_nama."</option>";
          }
          echo "</select>";
          ?>
          <div class="dropDownSelect2"></div>
        </div>
      </div>
      <div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
        <span class="label-input10">NAMA RACE *</span>
        <input class="input100" type="text" name="race" placeholder="Tulis Nama Race" required>
      </div>
      <div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
        <span class="label-input10">PHOTO *</span>
        <input class="input100" type="file" name="userfile" required>
      </div>

      <div class="container-contact100-form-btn">
        <button class="contact100-form-btn">
          <span>
            Submit
            <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
          </span>
        </button>
      </div>
    </form>
  </div>
</div>

<br>
            <?php $this->load->view('peserta/footer1') ?>
            <script src="<?php echo base_url().'assets/vendor/jquery/jquery-3.2.1.min.js'?>"></script>
            <script src="<?php echo base_url().'assets/vendor/animsition/js/animsition.min.js'?>"></script>
            <script src="<?php echo base_url().'assets/vendor/bootstrap/js/popper.js'?>"></script>
            <script src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
<!--===============================================================================================-->
            <script src="<?php echo base_url().'assets/vendor/select2/select2.min.js'?>"></script>
             <script>
  $(".js-select2").each(function(){
   $(this).select2({
    minimumResultsForSearch: 20,
    dropdownParent: $(this).next('.dropDownSelect2')
   });
  })
 </script>
        </body>

        </html>